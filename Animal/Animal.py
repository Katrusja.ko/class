class Animal:

    def __init__(self, name: str, appetite: int, is_hungry: bool = True) -> None:
        self.name = name
        self.appetite = appetite
        self.is_hungry = is_hungry

    def print_name(self) -> str:
        print(f"Hello, I'm {self.name}, {self.is_hungry}")

    def feed(self) -> int:
        if self.is_hungry:
            print(f"Eating {self.appetite} food points...")
            self.is_hungry = False
            return self.appetite
        else:
            return 0


class Cat(Animal):

    def __init__(self, name: str, is_hungry: bool = True) -> None:
        super().__init__(name, is_hungry)
        self.appetite = 3
        print("h", self.is_hungry)

    def catch_mouse(self) -> None:
        print("The hunt began!")


class Dog(Animal):

    def __init__(self, name: str, is_hungry: bool = True) -> None:
        super().__init__(name, is_hungry)
        self.appetite = 3
        print("h", self.is_hungry)


# lion = Animal("Lion", 25, False)
# lion.print_name()  # "Hello, I'm Lion"
# food_points = lion.feed()  # "Eating 25 food points..."
# print(food_points)  # 25
# print(lion.is_hungry)  # False
# print(lion.feed())  # 0

# cat = Cat("Cat")
# cat.print_name()  # "Hello, I'm Cat"
# cat.feed()  # "Eating 3 food points"

cat2 = Cat("Cat", False)
cat2.print_name()
print(cat2.feed())  # 0
print(cat2.is_hungry)  # False
cat2.catch_mouse()  # "The hunt began!"