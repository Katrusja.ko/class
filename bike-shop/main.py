from __future__ import annotations


class Bike:
    def __init__(self, brand: str, model: str, max_speed: int) -> None:
        self.brand = brand
        self.model = model
        self.max_speed = max_speed
        print(f"call from Bike but class is {type(self)}")

    @staticmethod
    def from_dict(bike_dict: dict) -> Bike:
        return Bike(
            brand=bike_dict["brand"],
            model=bike_dict["model"],
            max_speed=bike_dict["max_speed"],
        )


# class MountainBike:
class MountainBike(Bike):

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        print(f"call from MountainBike but class is {type(self)}")


# class RoadBike:
class RoadBike(Bike):

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
