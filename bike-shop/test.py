import ast
import inspect

import pytest

import bike_shop


@pytest.mark.parametrize("class_name", ["Bike", "MountainBike", "RoadBike"])
def test_classes_should_be_defined(class_name):
    assert (
        hasattr(bike_shop, class_name) is True
    ), f"Class '{class_name}' should be defined"


@pytest.mark.parametrize(
    "class_name,methods",
    [
        ("Bike", ["from_dict", "__init__"]),
        ("MountainBike", ["from_dict", "__init__"]),
        ("RoadBike", ["from_dict", "__init__"]),
    ],
)
def test_classes_should_have_corresponding_methods(class_name, methods):
    class_to_test = getattr(bike_shop, class_name)
    for method in methods:
        assert (
            hasattr(class_to_test, method) is True
        ), f"Class '{class_to_test}' should have method {method}"


@pytest.mark.parametrize(
    "class_name,method",
    [
        ("RoadBike", "__init__"),
        ("MountainBike", "__init__"),
    ],
)
def test_only_one_method_should_be_declared_in_each_of_children_classes(
    class_name, method
):
    class_to_test = getattr(bike_shop, class_name)
    class_source = inspect.getsource(class_to_test)
    parsed_class = ast.parse(class_source)
    assert [name.id for name in parsed_class.body[0].bases] == [
        "Bike"
    ], f"'{class_name}' should be inherited from 'Bike'"
    assert (
        len(parsed_class.body[0].body) == 1
    ), f"Only one method '{method}' should be defined inside class '{class_name}'"
    assert (
        parsed_class.body[0].body[0].name == method
    ), f"Only one method '{method}' should be defined inside class '{class_name}'"
    assert (
        len(parsed_class.body[0].body[0].body) == 1
    ), f"Only one command should be inside '{method}'"


@pytest.mark.parametrize(
    "bike_dict,brand,model,max_speed",
    [
        (
            {"brand": "Alchemy Bikes", "model": "A1", "max_speed": 20},
            "Alchemy Bikes",
            "A1",
            20,
        ),
        (
            {"brand": "Bilenky", "model": "B1", "max_speed": 45},
            "Bilenky",
            "B1",
            45,
        ),
    ],
)
def test_mountain_bike_from_dict_method(bike_dict, brand, model, max_speed):
    mountain_bike = bike_shop.MountainBike.from_dict(bike_dict)
    assert isinstance(mountain_bike, bike_shop.MountainBike), (
        "Bike that is created using 'MountainBike.from_dict' method "
        "should have 'MountainBike' type"
    )
    assert mountain_bike.brand == brand
    assert mountain_bike.model == model
    assert mountain_bike.max_speed == max_speed


@pytest.mark.parametrize(
    "bike_dict,brand,model,max_speed",
    [
        (
            {"brand": "Huffy", "model": "C 2000", "max_speed": 30},
            "Huffy",
            "C 2000",
            30,
        ),
        (
            {"brand": "Civia", "model": "P222", "max_speed": 35},
            "Civia",
            "P222",
            35,
        ),
    ],
)
def test_road_bike_from_dict_method(bike_dict, brand, model, max_speed):
    road_bike = bike_shop.RoadBike.from_dict(bike_dict)
    assert isinstance(road_bike, bike_shop.RoadBike), (
        "Bike that is created using 'RoadBike.from_dict' method "
        "should have 'RoadBike' type"
    )
    assert road_bike.brand == brand
    assert road_bike.model == model
    assert road_bike.max_speed == max_speed
